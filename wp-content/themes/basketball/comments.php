<?php
/**
 * @package Basketball
 **/
if( post_password_required() ){
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	if( have_comments() ):
		// have comments
		?>
        <h3 class="comment-title">
			<?php
			printf(
				esc_html( _nx( 'One comment on &ldquo;%2$s&rdquo;', '%1$s comments on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'basketball' ) ),
				number_format_i18n( get_comments_number() ),
				'<span>' . get_the_title() . '</span>'
			);
			?>
        </h3>

		<?php single_post_pagination(); ?>

<!--    <ol class="comment-list">-->
        <?php
        $args = array(
            'walker'            => new CommentWalker(),
            'max_depth'         => '',
            'style'             => 'ol',
            'callback'          => null,
            'end-callback'      => null,
            'type'              => 'all',
            'reply'             => 'Reply',
            'page'              =>  '',
            'per_page'          =>  '',
            'avatar_size'       =>  32,
            'reverse_top_level' => null,
            'format'            => 'html5',
            'short_ping'        => false,
            'echo'              => true
        );
        wp_list_comments( $args );
        ?>
<!--    </ol>-->

		<?php
            if( !comments_open() && get_comments_number() ):
                ?>
                <p class="no-comments">
                    <?php esc_html_e("Comments are closed.", "basketball"); ?>
                </p>
        <?php
		    endif;
		?>

	<?php
	endif;
	?>

	<?php
        $fields = array(
            'author'    =>  '<div class="col-sm-6">'
                                .'<div class="form-group">'
                                    .'<input class="form-control" name="author" id="author" type="text" placeholder="Name" value="'.esc_attr( $commenter['comment_author'] ).'">'
                                .'</div>'
                            .'</div>',
            'email'     => '<div class="col-sm-6">'
                               .'<div class="form-group">'
                                    .'<input class="form-control" name="email" id="email" type="email" value="'. esc_attr( $commenter['comment_author_email'] ).'" placeholder="Email">'
                               .'</div>'
                           .'</div>',

            'url'       => '<div class="col-12">'
                               .'<div class="form-group">'
                                    .'<input class="form-control" name="website" id="website" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" placeholder="Website">'
                               .'</div>'
                           .'</div>'
                           .'</div>',
        );
        $args = array(
            'class_submit'  => 'button button-contactForm',
            'label_submit'  => __( 'Submit Comment' ),
            'comment_field' => '<div class="row">'
                                   .'<div class="col-12">'
                                       .'<div class="form-group">'
                                            .'<textarea class="form-control w-100" required="required" name="comment" id="comment" cols="30" rows="9" placeholder="Write Comment"></textarea>'
                                       .'</div>'
                                   .'</div>',
            'fields'        => apply_filters( 'comment_form_default_fields', $fields )
        );
        comment_form( $args );
	?>
</div>  <!--./comments-area -->


