<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package basketball
 * @subpackage basketball
 */

get_header();
?>

<?php 
    if(have_posts()):
        while( have_posts() ):
            the_post();
            get_template_part( 'template-parts/single','post' );
        endwhile;
    endif; 
?>