<?php
/**
 * Basketball functions and definitions
 *
 * @package Basketball
 * @subpackage Basketball
 * @since 1.0.0
*/
require( 'inc/supports/theme_support.php' );
require( 'inc/supports/scripts_support.php' );
require( 'inc/supports/sidebar_supports.php' );

function defaultMainMenu(){
    	$context = '<ul>';
	    $context .= '<li class="nav-item"><a href="'. esc_url( home_url() ) .'" class="nav-link">Home</a></li>';
	    $context .= '<ul>';
	    echo $context;
    }

// Numbered Pagination
if ( !function_exists( 'post_pagination' ) ) {
	function post_pagination() {
		
		$prev_arrow = is_rtl() ? '>' : '<';
		$next_arrow = is_rtl() ? '<' : '>';
		
		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
            if( !$current_page = get_query_var('paged') )
                $current_page = 1;
            if( get_option('permalink_structure') ) {
                $format = 'page/%#%/';
            } else {
                $format = '&paged=%#%';
            }
            echo paginate_links(array(
                'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format'		=> $format,
                'current'		=> max( 1, get_query_var('paged') ),
                'total' 		=> $total,
                'mid_size'		=> 3,
                'type' 			=> 'list',
                'prev_text'		=> $prev_arrow,
                'next_text'		=> $next_arrow,
                ) );
		}
	}
}

if( !function_exists( 'linkReplace' ) ){
	function linkReplace( $url ){
		return str_replace( home_url(), "", esc_url($url) );
	}
}

require( 'inc/supports/post_support.php' );
require( 'inc/supports/breadcrums.php' );
require( 'inc/class/CommentWalker.php' );
require( 'inc/class/BasketBallNavWalker.php' );
require( 'Redux/ReduxCore/framework.php' );

//include redux framework core file
require( 'Redux/sample/redux-options.php' );
