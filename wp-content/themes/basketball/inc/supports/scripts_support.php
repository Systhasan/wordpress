<?php

/***
 * @desc Enqueue theme script and style
 * @params null
 * @return null
 * @author Muhammad hasan
 * @created 03th June 2019
 ***/
function basketball_script(){
	wp_register_style('bootstrap', get_template_directory_uri().'/assets/css/bootstrap.min.css');
	wp_register_style('fontawesome', get_template_directory_uri().'/assets/css/animate.css');
	wp_register_style('plugins', get_template_directory_uri().'/assets/css/owl.carousel.min.css');
	wp_register_style('rypp', get_template_directory_uri().'/assets/css/themify-icons.css');
	wp_register_style('flaticon', get_template_directory_uri().'/assets/css/flaticon.css');
	wp_register_style('magnific', get_template_directory_uri().'/assets/css/magnific-popup.css');
	wp_register_style('swiper', get_template_directory_uri().'/assets/css/swiper.min.css');
	wp_register_style('mainstyle', get_template_directory_uri().'/assets/css/style.css');
	wp_register_style('themestyle', get_template_directory_uri().'/style.css');


	wp_register_script('jquery-1-12',get_template_directory_uri().'/assets/js/jquery-1.12.1.min.js',array(),false,true);
	wp_register_script('popperjs', get_template_directory_uri().'/assets/js/popper.min.js',array(),false,true);
	wp_register_script('bootstrapjs',get_template_directory_uri().'/assets/js/bootstrap.min.js',array(),false,true);
	wp_register_script('aos',get_template_directory_uri().'/assets/js/aos.js',array(),false,true);
	wp_register_script('magnificjs',get_template_directory_uri().'/assets/js/jquery.magnific-popup.js',array(),false,true);
	wp_register_script('swiperjs',get_template_directory_uri().'/assets/js/swiper.min.js',array(),false,true);
	wp_register_script('masonry',get_template_directory_uri().'/assets/js/masonry.pkgd.js',array(),false,true);
	wp_register_script('owl-carousel-js',get_template_directory_uri().'/assets/js/owl.carousel.min.js',array(),false,true);
	wp_register_script('custom-js',get_template_directory_uri().'/assets/js/custom.js',array(),false,true);
	wp_register_script('script-js',get_template_directory_uri().'/assets/js/script.js',array(),false,true);

	wp_enqueue_style('bootstrap');
	wp_enqueue_style('fontawesome');
	wp_enqueue_style('plugins');
	wp_enqueue_style('rypp');
	wp_enqueue_style('flaticon');
	wp_enqueue_style('magnific');
	wp_enqueue_style('swiper');
	wp_enqueue_style('mainstyle');
	wp_enqueue_style('themestyle');

	wp_enqueue_script('jquery-1-12');
	wp_enqueue_script('popperjs');
	wp_enqueue_script('bootstrapjs');
	wp_enqueue_script('aos');
	wp_enqueue_script('magnificjs');
	wp_enqueue_script('swiperjs');
	wp_enqueue_script('masonry');
	wp_enqueue_script('owl-carousel-js');
	wp_enqueue_script('custom-js');
	wp_enqueue_script('script-js');
}
add_action('wp_enqueue_scripts', 'basketball_script');