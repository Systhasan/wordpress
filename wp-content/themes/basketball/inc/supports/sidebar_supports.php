<?php

if( (! function_exists('basketball_right_sidebar') ) ){
	function basketball_right_sidebar(){
		$args = array(
			'name'          => 'Right Sidebar',
			'description'   => 'This widgets are display in your right side.',
			'id'            => 'right-sidebar',
			'before_widget' => '<aside  id="%1$s" class="single_sidebar_widget post_category_widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget_title">',
			'after_title'   => '</h4>'
		);
		register_sidebar($args);
	}
}
add_action('widgets_init', 'basketball_right_sidebar');