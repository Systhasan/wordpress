<?php

if( !function_exists( 'basketball_breadcrumb' )  ){
	function basketball_breadcrumb() {
		// is home or front page.
		if ( !is_front_page() ) {
			echo '<p> <a href="';
			echo get_option('home');
			echo '">';
			echo 'Home';
			echo "</a> <span>/</span> ";
		}
		//	Is category or single page
		if ( is_category() || is_single() ) {
			$category = get_the_category();
			$ID = $category[0]->cat_ID;
			if( $ID ){
				// echo get_category_parents($ID, TRUE, ' &raquo; ', FALSE );
				echo get_category_parents($ID, TRUE, '', FALSE );
			}
		}
		// if(is_single() || is_page()) {the_title();}
		if(is_tag()){ echo "Tag: ".single_tag_title('',FALSE); }
		if(is_404()){ echo "404 - Page not Found"; }
		if(is_search()){ echo "Search"; }
		if(is_year()){ echo get_the_time('Y'); }
		echo "</p>";
	}
}

if (! function_exists('__get_category_name') ){
	function __get_category_name(){
		//	Is category
		if ( is_category() ) {
			$category = get_the_category();
			$ID = $category[0]->cat_ID;
			if( $ID ){
				echo get_category_parents($ID, false, '', FALSE );
			}
		}
	}
}

