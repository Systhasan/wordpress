<?php
/***
 * @desc  Cut long text to short text in given range.
 * @params string content, int limit
 * @return string
 * @author Muhammad hasan
 * @created 03th June 2019
 ***/
if( !function_exists('wordCutter') ) {
	function wordCutter( $content=null, $limit){
	$text = explode(" ", get_the_content() );
	$text = array_slice($text, 0, $limit);
	$text = implode(" ", $text). ' ... ';
	return $text;
}
}

/***
 * @desc Print post tags list with comma separate.
 * @params null
 * @return null
 * @author Muhammad hasan
 * @created 03th June 2019
 ***/
if( !function_exists( '__get_post_tags' ) ) {
	function __get_post_tags(){
		if( get_the_tags() ):
			foreach(get_the_tags() as $tag):
				$tag_names[] = $tag->name;
			endforeach;
			echo implode(', ', $tag_names);
		endif;
	}
}
// comment pagination
function single_post_pagination(){
	if( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		require ( get_template_directory() . '/template-parts/comment-pagination.php' );
	endif;
}
