<?php

/***
 * @desc theme supports
 * @params null
 * @return null
 * @author Muhammad hasan
 * @created 03th June 2019
 ***/
function themeSupports(){
	// load text domain
	load_theme_textdomain('basketball', get_template_directory_uri().'/languages' );
	// support for post thumbnail.
	add_theme_support('post-thumbnails' );
	// Activate HTML5 Support
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	// support for menu.
	register_nav_menu( 'main-menu', 'Main Menu' );
}
add_action('after_setup_theme', 'themeSupports');
