<?php

class CommentWalker extends Walker_Comment {

	protected function html5_comment( $comment, $depth, $args ) {
		//echo $args['avatar_size']; exit();
		?>
        <li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? 'comment-list' : 'comment-list' ); ?>>
            <div class="single-comment justify-content-between d-flex">
                <div class="user justify-content-between d-flex">
					<?php if ( 0 != $args['avatar_size'] ): ?>
                        <div class="thumb">
                            <img src="<?php echo get_avatar_url( $comment, $args['avatar_size'] ); ?>">
                        </div>
					<?php endif; ?>

                    <div class="desc">
                        <p class="comment">
							<?php echo get_comment_text(); ?>
                        </p>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-items-center">
								<?php printf( '<h5>%s</h5>', get_comment_author_link() ); ?>
                                <p class="date">
									<?php printf( _x( '%1$s at %2$s', '1: date, 2: time' ), get_comment_date(), get_comment_time() ); ?>
                                </p>
                            </div>

                            <div class="reply-btn">
								<?php
                                    $my_class = 'btn-reply text-uppercase';
                                        echo preg_replace( '/comment-reply-link/', 'comment-reply-link ' . $my_class, get_comment_reply_link( array_merge( $args, array(
                                            'add_below' => 'div-comment',
                                            'depth'     => $depth,
                                            'max_depth' => $args['max_depth']
                                        ) ) ), 1
                                    );
								?>
                            </div> <!-- ./reply-btn -->

                        </div>

						<?php if ( '0' == $comment->comment_approved ) : ?>
                            <p class="comment-awaiting-moderation label label-info"><?php _e( 'Your comment is awaiting moderation.' ); ?></p>
						<?php endif; ?>

                    </div>  <!-- ./desc -->

                </div>
            </div>
        </li>
		<?php
	}
}