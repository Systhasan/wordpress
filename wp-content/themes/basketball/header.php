<?php
/**
 * The header for our theme
 *
 * @package Basketball
 * @subpackage Basketball
 * @since 1.0.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?> >

<head>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo bloginfo('title')?></title>
    <link rel="icon" href="img/favicon.png">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
    <?php global $basket; ?>
    <!--::header part start::-->
    <header class="header_area">
        <div class="sub_header">
            <div class="container">
                <div class="row align-items-center">
                  <div class="col-md-4 col-xl-6">
                      <?php if( $basket['header-logo']['url'] ) :?>
                          <div id="logo">
                              <a href="<?php echo esc_url( home_url() ); ?>">
                                  <img src="<?php echo $basket['header-logo']['url'];?>"
                                       class="img-fluid"
                                       alt="<?php echo $basket['header-logo-title'];?>"
                                       style="height:<?php echo $basket['header-logo-height'];?>px ; width:<?php echo $basket['header-logo-width'];?>px ;"
                                  />
                              </a>
                          </div>
                      <?php endif; ?>
                  </div>
                  <div class="col-md-8 col-xl-6">
                      <div class="sub_header_social_icon float-right">
                        <a href="tel:<?php echo $basket['phone-number']; ?>"><i class="flaticon-phone"></i><?php echo $basket['phone-number']; ?></a>

                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_menu">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <?php
                                    $args = array(
                                        'container'         => '',
                                        'theme_location'    => 'main-menu',
                                        'menu_class'        => 'navbar-nav mr-auto',
                                        'walker'            => new BasketBallNavWalker()
                                    );
                                    wp_nav_menu($args);
                                ?>



                                <div class="header_social_icon d-none d-lg-block">
                                    <ul>
	                                    <?php if($basket['facebook-url']): ?>
                                            <li><a href="<?php echo linkReplace($basket['facebook-url']);  ?>"><i class="ti-facebook"></i></a></li>
	                                    <?php endif; ?>
	                                    <?php if($basket['twitter-url']): ?>
                                            <li><a href="<?php echo linkReplace($basket['twitter-url']);  ?>"> <i class="ti-twitter"></i></a></li>
	                                    <?php endif; ?>
	                                    <?php if($basket['instagram-url']): ?>
                                            <li><a href="<?php echo linkReplace($basket['instagram-url']);  ?>"><i class="ti-instagram"></i></a></li>
	                                    <?php endif; ?>
	                                    <?php if($basket['pinterest-url']): ?>
                                            <li><a href="<?php echo linkReplace($basket['pinterest-url']);  ?>"><i class="ti-pinterest"></i></a></li>
	                                    <?php endif; ?>
	                                    <?php if($basket['youtube-url']): ?>
                                            <li><a href="<?php echo linkReplace($basket['youtube-url']);  ?>"><i class="ti-youtube"></i></a></li>
	                                    <?php endif; ?>
	                                    <?php if($basket['snapchat-url']): ?>
                                            <li><a href="<?php echo linkReplace($basket['snapchat-url']);  ?>"><i class="ti-snapchat"></i></a></li>
	                                    <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </nav>

                        <div class="header_social_icon d-block d-lg-none">
                            <ul>
	                            <?php if($basket['facebook-url']): ?>
                                    <li><a href="<?php echo linkReplace($basket['facebook-url']);  ?>"><i class="ti-facebook"></i></a></li>
	                            <?php endif; ?>
	                            <?php if($basket['twitter-url']): ?>
                                    <li><a href="<?php echo linkReplace($basket['twitter-url']);  ?>"> <i class="ti-twitter"></i></a></li>
	                            <?php endif; ?>
	                            <?php if($basket['instagram-url']): ?>
                                    <li><a href="<?php echo linkReplace($basket['instagram-url']);  ?>"><i class="ti-instagram"></i></a></li>
	                            <?php endif; ?>
	                            <?php if($basket['pinterest-url']): ?>
                                    <li><a href="<?php echo linkReplace($basket['pinterest-url']);  ?>"><i class="ti-pinterest"></i></a></li>
	                            <?php endif; ?>
	                            <?php if($basket['youtube-url']): ?>
                                    <li><a href="<?php echo linkReplace($basket['youtube-url']);  ?>"><i class="ti-youtube"></i></a></li>
	                            <?php endif; ?>
	                            <?php if($basket['snapchat-url']): ?>
                                    <li><a href="<?php echo linkReplace($basket['snapchat-url']);  ?>"><i class="ti-snapchat"></i></a></li>
	                            <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->