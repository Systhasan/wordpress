<!--::breadcrumb part start::-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner">
                    <div class="breadcrumb_iner_item">
                        <h1><?php the_title(); ?></h1>
                        <?php basketball_breadcrumb(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--::breadcrumb part start::-->


<!--================Blog Area =================-->
<section class="blog_area single-post-area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-list">
                <div class="single-post">
                    <!--had post thumbnail-->
                    <?php if(has_post_thumbnail()):?>
                        <div class="feature-img">
                            <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url();?>" alt="">
                        </div>
                    <?php endif; ?>

                    <div class="blog_details">
                        <!--Display Post Title-->
                        <h2> <?php echo the_title(); ?> </h2>
                        <ul class="blog-info-link mt-3 mb-4">
                            <li><a href="#"><i class="far fa-user"></i> Travel, Lifestyle</a></li>
                            <li>
                                <a><i class="far fa-comments"></i>
	                                <?php
	                                    printf(_nx('%1$s Comment', '%1$s Comments', get_comments_number(), 'Comment Number', 'basketball'), number_format_i18n( get_comments_number() ));
	                                ?>
                                </a>
                            </li>
                        </ul>
                        <!--Display post content-->
                        <p class="excert">
                            <?php echo the_content(); ?>
                        </p>
                    </div>
                </div>
                <div class="navigation-top">
                    <div class="d-sm-flex justify-content-between text-center">
                        <p class="like-info"><span class="align-middle"><i class="far fa-heart"></i></span> Lily and 4
                            people like this</p>
                        <div class="col-sm-4 text-center my-2 my-sm-0">
                            <!-- <p class="comment-count"><span class="align-middle"><i class="far fa-comment"></i></span> 06 Comments</p> -->
                        </div>
                        <ul class="social-icons">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                        </ul>
                    </div>

                    <div class="navigation-area">
                        <div class="row">
                            <!--Previous Post title, image and URL-->
                            <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                <?php if( strlen(get_previous_post()->post_title) > 0 ): ?>
                                    <div class="thumb">
                                        <a href="<?php echo get_permalink(get_previous_post()->ID); ?>">
                                            <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url( get_previous_post()->ID, 'thumbnail' ); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="arrow">
                                        <a href="<?php echo get_permalink(get_previous_post()->ID);?>">
                                            <span class="lnr text-white ti-arrow-left"></span>
                                        </a>
                                    </div>
                                    <div class="detials" >
                                        <p>Prev Post</p>
                                        <?php previous_post_link('<h4>%link</h4>'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>


                            <!--Next Post title, image and url-->
                            <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                                <?php if( strlen(get_next_post()->post_title) > 0 ): ?>
                                    <div class="detials">
                                         <p>Next Post</p>
                                         <?php next_post_link('<h4>%link</h4>')?>
                                    </div>
                                    <div class="arrow">
                                        <a href="#">
                                            <span class="lnr text-white ti-arrow-right"></span>
                                        </a>
                                    </div>
                                    <div class="thumb">
                                    <a href="<?php echo get_permalink(get_next_post()->ID); ?>">
                                        <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(get_next_post()->ID, 'thumbnail'); ?>" alt="">
                                    </a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Comments Template -->
                <?php if( comments_open() ): ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
                <!-- /Comments Template -->

            </div>      <!-- ./col-lg-8 posts-list -->

	        <?php get_sidebar(); ?>

        </div>      <!-- ./row -->
    </div>      <!-- ./container -->
</section>

<!--================Blog Area end =================-->