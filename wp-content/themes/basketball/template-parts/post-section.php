<article class="blog_item">
    <div class="blog_item_img">

        <?php if( has_post_thumbnail() ): ?>
            <img class="card-img rounded-0" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>" >
        <?php endif; ?>


    <a class="blog_item_date">
        <h3>
            <?php echo the_time( 'd' ); ?>
        </h3>
        <p>
            <?php echo the_time( 'M' ); ?>
        </p>
    </a>

    </div>

    <div class="blog_details">
        <a class="d-inline-block" href="<?php echo get_the_permalink()?>">
            <h2> <?php echo the_title(); ?> </h2>
        </a>
        <p>
            <?php echo wordCutter(null, 20); ?>
        </p>
        <ul class="blog-info-link">
            <li>
                <a href="#"><i class="far fa-user"></i>

                    <?php __get_post_tags(); ?>

                </a>
            </li>
            <li>
                <a href="#"><i class="far fa-comments"></i>
                    <?php echo  comments_popup_link("0 Comment", "1 Comment", "% Comments", '', '') ;?>
                </a>
            </li>
        </ul>
    </div>
</article>
