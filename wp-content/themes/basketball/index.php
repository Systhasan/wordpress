<?php
/**
 * The main template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Basketball
 * @subpackage Basketball
 * @since 1.0.0
 */
 get_header();
?>

    <?php global $basket; ?>

    <!--::breadcrumb part start::-->
    <?php if($basket['header-image']): ?>
    <section class="breadcrumb breadcrumb_bg"
             <?php if( $basket['section-image']['url'] ): ?>
                style="background-image: url(<?php echo $basket['section-image']['url']; ?>);"
             <?php else: ?>
                 style="background-image: url(<?php echo get_stylesheet_directory_uri() . '/assets/img/banner_bg.png'; ?>);"
            <?php endif; ?>
    >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <h1 style="color: <?php echo $basket['section-image-color']; ?>">
                                <?php __get_category_name();?>
                            </h1>
                            <?php basketball_breadcrumb(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!--::breadcrumb part start::-->

  

    <!--================Blog Area =================-->
        <section class="blog_area section_padding">
            <div class="container">
                <div class="row">
                        <div class="col-lg-8 mb-5 mb-lg-0">
                            <div class="blog_left_sidebar">

                            <?php if( have_posts() ): 
                                while( have_posts() ):
                                    the_post();
                                    get_template_part( 'template-parts/post','section' );
                                endwhile;
                            endif; ?>  


                            <!--Page pagination-->
                            <nav class="blog-pagination justify-content-center d-flex">
                                <?php post_pagination(); ?>
                            </nav>
                            
                        

                            </div>
                        </div>

	                <?php get_sidebar(); ?>
                </div>
            </div>
        </section>
    <!--================Blog Area =================-->


<?php get_footer(); ?>